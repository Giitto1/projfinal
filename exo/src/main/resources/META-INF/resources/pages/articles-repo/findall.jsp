<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>

<!-- special jstl -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>findall</title>
</head>
<body>

	<div class="container">
		<h2>Liste de tous les articles</h2>
		<br>


		<table class=" table table-striped">

			<!-- Head du tableau -->
			<thead>
				<tr>
					<!-- 			Ici, on adapte les noms de colonnes en fonction du choix de la langue qu'on effectue dans la partie mvc-servlet -->
					<!-- 			xml -->


					<th><spring:message code="article.list.id"></spring:message></th>
					<th><spring:message code="article.list.marque"></spring:message></th>
					<th><spring:message code="article.list.prix"></spring:message></th>
					<th><spring:message code="article.list.description"></spring:message></th>


				</tr>
			</thead>
			<tbody>
				<c:forEach var="article" items="${lst_articles}">

					<tr>
						<td>${article.id}</td>
						<td>${article.marque}</td>
						<td>${article.prix}</td>
						<td>${article.description}</td>
					</tr>
				</c:forEach>


			</tbody>
		</table>
	</div>
</body>
</html>
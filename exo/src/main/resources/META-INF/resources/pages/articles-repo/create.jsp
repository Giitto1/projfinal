<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EcranDeCreation</title>
</head>
<body>

	<div class="container">
		<h3>Ajouter un article</h3>
		<p>Veuillez remplir les champs suivants pour ajouter un nouvel
			article.</p>
		<form:form action="" method="post" modelAttribute="article"
			class="was-validated">
			<div class="form-group">
				<label for="id">Identifiant :</label> <input type="number"
					class="form-control" id="id"
					placeholder="Identifiant de l'article, il doit �tre unique!"
					name="id" required>
				<div class="valid-feedback">Valide.</div>
				<div class="invalid-feedback">Veuillez remplir ce champ.</div>
			</div>
			<div class="form-group">
				<label for="marque">Marque :</label> <input type="text"
					class="form-control" id="marque" placeholder="Marque de l'article"
					name="marque" required>
				<div class="valid-feedback">Valide.</div>
				<div class="invalid-feedback">Veuillez remplir ce champ.</div>
			</div>
			<div class="form-group">
				<label for="prix">Prix :</label> <input type="number"
					class="form-control" id="prix" placeholder="Prix de l'article"
					name="prix" required>
				<div class="valid-feedback">Valide.</div>
				<div class="invalid-feedback">Veuillez remplir ce champ.</div>
			</div>
			<div class="form-group">
				<label for="description">Description :</label> <input type="text"
					class="form-control" id="description"
					placeholder="Type du produit, quantit�, �ge recommand�"
					name="description" required>
				<div class="valid-feedback">Valide.</div>
				<div class="invalid-feedback">Veuillez remplir ce champ.</div>
			</div>
			<div class="container">
				<button type="submit" class="btn btn-success">Ajouter</button>
			</div>
		</form:form>


		<!-- 		<h3>Pour afficher la liste des articles, cliquer sur le bouton ci-dessous.</h3> -->
		<%-- 		<form:form action="" method="post" modelAttribute="article"> --%>
		<!-- 		<button type="submit" class="btn btn-secondary">Ajouter</button> -->
		<%-- 		</form:form> --%>

	</div>




</body>
</html>
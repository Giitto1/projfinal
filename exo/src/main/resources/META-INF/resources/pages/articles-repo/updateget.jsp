<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EcranDeMiseAJour</title>
</head>
<body>

	<div class="container">
		<h3>Mettre � Jour un Article</h3>


		<form:form action="" method="post" modelAttribute="article">
			<div class="form-group">

				<form:label path="id">Identifiant:</form:label>
				<form:input path="id" readonly="true" class="form-control" />
			</div>
			<div class="form-group">
				<form:label path="marque">Marque:</form:label>
				<form:input path="marque" class="form-control" />
			</div>
			<div class="form-group">
				<form:label path="prix">Prix:</form:label>
				<form:input path="prix" class="form-control" />
			</div>
			<div class="form-group">
				<form:label path="description">Description:</form:label>
				<form:input path="description" class="form-control" />

			</div>

			<form:hidden path="version" />

			<br>
			<br>
			<div class="container">
				<button type="submit" name="save" class="btn btn-secondary">Modifier</button>
			</div>
		</form:form>

	</div>
</body>
</html>
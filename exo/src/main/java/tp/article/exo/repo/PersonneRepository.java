package tp.article.exo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import tp.article.exo.model.Personne;

public interface PersonneRepository extends JpaRepository<Personne, String>{
	   Personne findByMailAndMdp(String mail, String mdp);


}

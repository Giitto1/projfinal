package tp.article.exo.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import tp.article.exo.model.Article;

public interface ArticleRepository extends JpaRepository<Article, Integer>{
	public List<Article> findByDescriptionContaining(String chaine);
	public List<Article> findByPrixBetween(int min, int max);
	public List<Article> findByPrixLessThan(int valeur);
	public Optional<Article> findFirstByMarque(String marque);
	public int countByMarque(String marque);
	public List<Article> findAllByOrderByMarqueAsc();
	public List<Article> findAllByOrderByIdAsc();
	public List<Article> findByMarque(String marque);

}

package tp.article.exo.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import tp.article.exo.model.Article;
import tp.article.exo.model.MinMax;
import tp.article.exo.repo.ArticleRepository;


@Controller
@RequestMapping("/jsp")
public class ArticleController {

	@Autowired
	private ArticleRepository articleRepository;

	@GetMapping("/hello")
	public String test() {
		return "hello";
	}
	// 1
	@GetMapping("/findall")
	public ModelAndView findAllRepo() {
		return new ModelAndView("articles-repo/findall", "lst_articles", articleRepository.findAll());
	}

	// 2

	@GetMapping("/findbyid")
	public ModelAndView findById() {
		return new ModelAndView("articles-repo/findbyid", "lst_articles", articleRepository.findAllByOrderByMarqueAsc());
	}

	@PostMapping("/findbyid")
	public ModelAndView findById(@ModelAttribute(name = "id") Integer id) {
		ArrayList<Article> lst = new ArrayList<Article>();
		Optional<Article> op = articleRepository.findById(id);
        Article a =null;
        if (op.isPresent()) {
            a = op.get();
        }
        lst.add(a);
		return new ModelAndView("articles-repo/findbyidpost", "lst_articles",
				lst);
	}

	// 3.
	@GetMapping("/create")
	public ModelAndView create() {
		return new ModelAndView("articles-repo/create", "article", new Article());
	}

	@PostMapping("/create")
	public String create(@ModelAttribute(name = "article") Article article) {
		articleRepository.save(article);
		return "articles-repo/createpost";
	}

//	// 4. On affiche les infos sur l'individu
//	@GetMapping("/update")
//	public ModelAndView list(@RequestParam(name = "id", required = true) Integer id) {
//		return new ModelAndView("articles-repo/updateget", "article", articleRepository.findById(id));
//	}
//
//	@PostMapping("/update")
//	public ModelAndView search(@ModelAttribute(name = "article") Article article) {
//		articleRepository.save(article);
//		return new ModelAndView("articles-repo/updatepost", "lst_articles", articleRepository.findAll());
//	}

	// @PostMapping("/update")
	// public ModelAndView save(@ModelAttribute(name = "article") Article
	// article) {
	// return new ModelAndView("articles-repo/updatepost", "article",
	// articleRepository.save(article));
	// }

	// articleRepository.findById(id).get())

	// 4. On affiche les infos sur l'individu
		@GetMapping("/update")
		public ModelAndView list() {
			return new ModelAndView("articles-repo/update", "lst_articles", articleRepository.findAll());
		}

		@PostMapping(value = "/update", params = "search")
		public ModelAndView search(@ModelAttribute(name = "id") Integer id) {
			//articleRepository.save(article);
			return new ModelAndView("articles-repo/updateget", "article", articleRepository.findById(id));
		}
		
		@PostMapping(value = "/update", params = "save")
		public ModelAndView save(@ModelAttribute(name = "article") Article article) {
			articleRepository.save(article);
			return new ModelAndView("articles-repo/updatepost", "lst_articles", articleRepository.findAllByOrderByIdAsc() );
		}
	
	// 5.
	@GetMapping("/delete")
	public ModelAndView delete() {

		return new ModelAndView("articles-repo/delete", "lst_articles", articleRepository.findAllByOrderByIdAsc());
	}

	@PostMapping("/delete")
	public ModelAndView delete(@ModelAttribute(name = "id") Integer id) {
		articleRepository.deleteById(id);
		return new ModelAndView("articles-repo/deletepost", "lst_articles", articleRepository.findAllByOrderByIdAsc());
	}

	// 6.
	@GetMapping("/findbyminmax")
	public ModelAndView findByMinMax() {
		return new ModelAndView("articles-repo/findbyminmax", "lst_articles", articleRepository.findAllByOrderByIdAsc());
	}

	@PostMapping("/findbyminmax")
	public ModelAndView findByMinMax(@ModelAttribute(name = "valeurs") MinMax minmax) {
		return new ModelAndView("articles-repo/findbyminmaxpost", "lst_articles",
				articleRepository.findByPrixBetween(minmax.getMin(), minmax.getMax()));
	}
	
	// 7. 
	@GetMapping("/findbydesccontain")
	public ModelAndView findByDesContain() {
		return new ModelAndView("articles-repo/findbydescont", "lst_articles", articleRepository.findAllByOrderByIdAsc());
	}

	@PostMapping("/findbydesccontain")
	public ModelAndView findByDesContain(@ModelAttribute(name = "chaine") String chaine) {
		return new ModelAndView("articles-repo/findbydescontpost", "lst_articles",
				articleRepository.findByDescriptionContaining(chaine));
	}
	
	// 8. 
		@GetMapping("/findbyprixlessthan")
		public ModelAndView findByPrixLess() {
			return new ModelAndView("articles-repo/findbyprixless", "lst_articles", articleRepository.findAllByOrderByIdAsc());
		}

		@PostMapping("/findbyprixlessthan")
		public ModelAndView findByPrixLess(@ModelAttribute(name = "prix") Integer prix) {
			return new ModelAndView("articles-repo/findbyprixlesspost", "lst_articles",
					articleRepository.findByPrixLessThan(prix));
		}
		

		// 9. 
			@GetMapping("/countbymarque")
			public ModelAndView countByMarque() {
				return new ModelAndView("articles-repo/countbymarque", "lst_articles", articleRepository.findAllByOrderByIdAsc());
			}
			
			@PostMapping(value = "/countbymarque", params = "filtre")
			public ModelAndView countByMarque2() {
				return new ModelAndView("articles-repo/countbymarque", "lst_articles", articleRepository.findAllByOrderByMarqueAsc());
			}

			@PostMapping(value="/countbymarque", params = "compte")
			public ModelAndView countByMarque(@ModelAttribute(name = "marque") String marque) {
				return new ModelAndView("articles-repo/countbymarquepost", "compte",
						articleRepository.countByMarque(marque));
			}
}

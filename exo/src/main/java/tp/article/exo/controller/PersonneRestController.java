package tp.article.exo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tp.article.exo.model.Article;
import tp.article.exo.model.Personne;
import tp.article.exo.repo.PersonneRepository;

@RestController
@RequestMapping("/personne")
public class PersonneRestController {
	@Autowired
	private PersonneRepository prepo;

	@CrossOrigin
	@GetMapping
	public List<Personne> list() {
		return this.prepo.findAll();
	}

	@CrossOrigin
	@PostMapping
	public ResponseEntity<Object> create(@RequestBody Personne personne) {
		if (!this.prepo.existsById(personne.getMail())) {
			this.prepo.save(personne);
			return new ResponseEntity<Object>(null, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
		}
	}

	@CrossOrigin
	@GetMapping("/{mail}/{mdp}")
	public Personne findByMailAndMdp(@PathVariable(name = "mail") String mail,
			@PathVariable(name = "mdp") String mdp) {
		Personne p = prepo.findByMailAndMdp(mail, mdp);
		return p;

	}

}

package tp.article.exo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import tp.article.exo.model.Article;
import tp.article.exo.repo.ArticleRepository;

@Service
public class ConsoleService implements CommandLineRunner {
	@Autowired
	ArticleRepository arepo;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("TP Articles");
		for (Article a: arepo.findAll())
		{
			System.out.println(a);
		}

	}

}
